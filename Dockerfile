FROM ubuntu:focal
LABEL maintainer="Addison Crump <me@addisoncrump.info>"

ENV GHIDRA_VERSION=ghidra_10.1.1_PUBLIC
ENV GHIDRA_URL="https://github.com/NationalSecurityAgency/ghidra/releases/download/Ghidra_10.1.1_build/ghidra_10.1.1_PUBLIC_20211221.zip"
ENV GHIDRA_WORK_DIR=/opt/ghidra
ENV Z3_VERSION=z3-4.8.13
ENV Z3_URL="https://github.com/Z3Prover/z3/releases/download/${Z3_VERSION}/${Z3_VERSION}-x64-glibc-2.31.zip"
ENV Z3_WORK_DIR=/opt/z3
ENV GHIDRA_INSTALL_DIR="$GHIDRA_WORK_DIR/$GHIDRA_VERSION"

# basic dependencies
RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt -y install build-essential curl git openjdk-17-jdk-headless unzip gcc-aarch64-linux-gnu build-essential mdm python3 python3-distutils
RUN apt clean autoclean && apt -y autoremove && bash -c "rm -rf /var/lib/{apt,dpkg,cache,log}/"

# ghidra
RUN mkdir -p "$GHIDRA_WORK_DIR"
RUN cd "$GHIDRA_WORK_DIR" && \
    curl -sL -o "$GHIDRA_VERSION.zip" "$GHIDRA_URL" && \
    unzip "$GHIDRA_VERSION.zip" && \
    rm "$GHIDRA_VERSION.zip" && \
    cd "$GHIDRA_INSTALL_DIR/support" && \
    ./buildGhidraJar

# z3
RUN mkdir -p "$Z3_WORK_DIR"
RUN cd "$Z3_WORK_DIR" && \
    curl -sL -o "$Z3_VERSION.zip" "$Z3_URL" && \
    unzip "$Z3_VERSION.zip" && \
    mv */bin/*.so */bin/*.a */bin/*.jar /usr/lib/
